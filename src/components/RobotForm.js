import React, { Component } from 'react'

class RobotForm extends Component{
    constructor(props){
        super(props);
        this.state={
            name:'',
            type:'',
            mass:''
        }
    }
     handleChangeName=(event)=>{
        this.setState({
            name:event.target.value
        });
    }
    
    handleChangeType=(event)=>{
        this.setState({
            type:event.target.value
        });
    }
    
    handleChangeMass=(event)=>{
        this.setState({
            mass:event.target.value
        });
    }
   
   render()
    {return(
        <div>
            
        <input type="text" id="name" onChange={this.handleChangeName} name="name" />
        <input type="text" id="type" onChange={this.handleChangeType} name="type" />
        <input type="text" id="mass" onChange={this.handleChangeMass} name="mass" />

        <input type="button" value="add" onClick={() => this.props.onAdd({
                name : this.state.name,
                type : this.state.type,
                mass : this.state.mass
            })} />
      
        </div>
     );
    }
    
}

export default RobotForm;